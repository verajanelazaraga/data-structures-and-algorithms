def bubblesort(num):
	swapped = False
	for n in range(len(num)-1, 0, -1):
		for i in range(n):
			if num[i] > num[i + 1]:
				swapped = True
				num[i], num[i + 1] = num[i + 1], num[i]	
		if not swapped:
			return

num = [10, 2, 13, 24, 36, 111, 43, 1]

print("Unsorted list is,")
print(num)
bubblesort(num)
print("Sorted Array is, ")
print(num)