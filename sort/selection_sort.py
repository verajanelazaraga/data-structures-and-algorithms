def selectionSort(array, size):
	
	for ind in range(size):
		min_index = ind

		for j in range(ind + 1, size):
			if array[j] < array[min_index]:
				min_index = j
		(array[ind], array[min_index]) = (array[min_index], array[ind])

arr = [-4, 34, 0, 12, -5,33,-91,-123,532]
size = len(arr)
selectionSort(arr, size)
print('After sorting in Ascending Order by Selected Sort, the array is:')
print(arr)