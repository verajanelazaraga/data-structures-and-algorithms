attempts = 1
position = -1


def binary_search(num, n):
    l = 0
    u = len(num) - 1

    while l <= u:
        mid = (l + u) // 2

        if num[mid] == n:
            globals()["position"] = mid
            return True
        else:
            if num[mid] < n:
                l = mid + 1
            else:
                u = mid - 1

    return False


values = input("Enter any comma-separated numbers here (no spaces):\n")
n = int(input("Please enter the number you're trying to find.: \n"))
num = values.split(",")

for i in range(0, len(num)):
    num[i] = int(num[i])

if binary_search(num, n):
    print("Found! Index #:", position)
else:
    print("not found")