from turtle import position


position = -1
attempts = 1

def linear_search(num, n):
    i = 0
    while i < len(num):
        if num[i] == n:
            globals()["position"] = i
            return True
        i = i + 1
        globals()["attempts"] = attempts + 1
    return False


values = input("Enter any comma-separated numbers here (no spaces):\n")
n = int(input("Please enter the number you're trying to find.: \n"))
num = values.split(",")

for i in range(0, len(num)):
    num[i] = int(num[i])


if linear_search(num, n):
    print("Found! Index #:", position)
    print("Took", attempts, "attempts to find the number")
else:
    print("not found")